KUBECTL IMAGE
-------------

Image contains just simple kubectl interface that able to connect to remote k8s clusters.

To call version of kubectl client:
```
kubectl version --client
```

To manage remote k8s cluster via kubectl interface:
```
docker run algeran/kubectl:v1.15.5-distroless \
kubectl config set-cluster k8s --server="$KUBE_URL" --insecure-skip-tls-verify=true \
# or pass your certificate file instead --insecure-skip-tls-verify=true - that is preferred
kubectl config set-credentials admin --token="$KUBE_TOKEN" \
kubectl config set-context default --cluster=k8s --user=admin \
kubectl config use-context default \
kubectl apply -f manifesto.yaml
```

or

```
docker run -v /path/to/.kube/config:/.kube/config algeran/kubectl:v1.15.5-distroless \
kubectl --kubeconfig='/.kube/config' \
kubectl apply -f manifesto.yaml
```

For more information look at [documentation](https://kubernetes.io/docs/tasks/access-application-cluster/configure-access-multiple-clusters/)
