FROM alpine:3.10 as builder
ARG KUBECTL_VERSION
RUN wget https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl \
    && chmod +x ./kubectl

FROM gcr.io/distroless/base
COPY --from=builder /kubectl /usr/local/bin/kubectl
